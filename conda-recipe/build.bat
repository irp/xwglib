set BOOST_ROOT="%PREFIX"
meson setup -Dprefix=%PREFIX% --cmake-prefix-path=%PREFIX% builddir 
cd builddir
meson compile
@REM meson test --no-rebuild --print-errorlogs
meson install --no-rebuild --destdir=%PREFIX%