export BOOST_ROOT=$PREFIX
meson setup -Dprefix=$PREFIX --cmake-prefix-path=$PREFIX builddir 
cd builddir
meson compile
# meson test --no-rebuild --print-errorlogs
meson install --no-rebuild