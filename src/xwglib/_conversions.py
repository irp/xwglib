import scipy.constants as const
import numpy as np


def energy_to_wavenumber(x):
    # in nm
    return x * 1e3 * (const.eV / const.hbar) / const.c / 1e9


def index_to_bandwidth(ind, energy):
    return energy * ind


def attenuation_length(ind, energy):
    # in nm
    k0 = energy_to_wavenumber(energy)
    return 0.5 / k0 / np.imag(ind)
