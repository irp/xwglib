import numpy as np
from collections import namedtuple
from ._conversions import energy_to_wavenumber


class Material:
    def __init__(self, name, color, index_fn):
        self.name = name
        self.color = color
        self.index_fn = index_fn

    def index(self, energy):
        return self.index_fn(energy)

    def as_const(self, energy):
        return ConstMaterial(self.name, self.color, self.index(energy))


class ConstMaterial(Material):
    def __init__(self, name, color, index):
        def index_fn(energy):
            return index

        super().__init__(name, color, index_fn)


class XRLMaterial(Material):
    def __init__(self, formula, color, density):
        import xraylib as xrl

        def index_fn(energy):
            # ignore additional keyword arguments
            return xrl.Refractive_Index(formula, energy, density)

        super().__init__(formula, color, index_fn)


Layer = namedtuple("Layer", ["material", "thickness"])


class LayerStack:
    def __init__(self, substrate, layers, superstrate=None):
        self.substrate = substrate
        self.layers = tuple(layers)
        if superstrate is not None:
            self.superstrate = superstrate
        else:
            self.superstrate = substrate

    def get_all_layers(self):
        return [self.substrate] + [layer.material for layer in self.layers] + [self.superstrate]

    def get_d_stack(self):
        return [layer.thickness for layer in self.layers]

    def get_n_stack(self, energy):
        layers = self.get_all_layers()

        return [m.index(energy) for m in layers]

    def get_layer_intervals(self, x0=0, infinity=50):
        d_stack = np.asarray(self.get_d_stack())

        x = np.empty((d_stack.size + 1,), dtype=float)
        x[0] = x0
        x[1:] = x0 + np.cumsum(d_stack)

        xj = [(x[0] - infinity, x[0])] + list(zip(x[:-1], x[1:])) + [(x[-1], x[-1] + infinity)]

        return xj

    def get_colors(self):
        materials = self.get_all_layers()

        return [m.color for m in materials]

    def plot_layers(self, ax, x0=0, infinity=50, title=None, vertical=False):
        intervals = self.get_layer_intervals(x0=x0, infinity=infinity)
        colors = self.get_colors()

        if not vertical:
            for x, m in zip(intervals, colors):
                ax.axvspan(*x, color=m, alpha=0.1)
        else:
            for x, m in zip(intervals, colors):
                ax.axhspan(*x, color=m, alpha=0.1)

        if title is not None:
            ax.set_title(title)

    def description(self, separator="/"):
        names = [m.name for m in self.get_all_layers()]

        return separator.join(names)

    def get_parameters(self, energy):
        d_stack = self.get_d_stack()
        n_stack = self.get_n_stack(energy)

        k0 = energy_to_wavenumber(energy)

        d = np.asarray(d_stack)
        n = np.asarray(n_stack)
        mu = np.ones_like(n)

        return d, n, mu, k0

    def get_thickness(self):
        return np.asarray(self.get_d_stack()).sum()
