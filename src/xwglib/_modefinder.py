import numpy as np
import cxroots
import numpy.ma as ma
import scipy.optimize as opt
from collections import namedtuple

from ._model import Model


class Modefinder:
    def __init__(self, model):
        if not isinstance(model, Model):
            raise ValueError
        self.model = model

    def find_modes(self, contour, polarization="TE", **kwargs):
        if polarization not in {"TE", "TM"}:
            raise ValueError

        if polarization == "TE":
            func = self.model.wronskian_te
            fprime = self.model.diff_wronskian_te
        else:
            func = self.model.wronskian_tm
            fprime = self.model.diff_wronskian_tm

        # search for roots of multiplicity 1
        rootresult = contour.roots(func, fprime, M=1, **kwargs)
        roots = rootresult[0]

        indices = self.model.mode_index(roots)
        modes_conformal = self.model.u_fn(roots)

        # sort the pair
        tmp = sorted(zip(indices, modes_conformal), key=lambda pair: pair[0].real, reverse=True)
        indices, modes_conformal = zip(*tmp)
        indices = np.asarray(indices)
        modes_conformal = np.asarray(modes_conformal)

        if self.model.conformal:
            types = self.mode_type(modes_conformal)
        else:
            types = self.mode_type(indices)

        modearray = ModeArray(modes_conformal, indices, types)

        return ModeResult(modearray, rootresult)

    def update_modes(self, guessed_modes, polarization="TE"):
        if polarization not in {"TE", "TM"}:
            raise ValueError
        modes = np.asarray(guessed_modes)
        roots = np.empty_like(modes)

        if polarization == "TE":
            func = self.model.wronskian_te
            fprime = self.model.diff_wronskian_te
        else:
            func = self.model.wronskian_tm
            fprime = self.model.diff_wronskian_tm

        for i, x0 in enumerate(modes):
            root, r = opt.newton(func, x0, fprime, full_output=True)
            roots[i] = root

        indices_new = self.model.mode_index(roots)
        modes_new = self.model.u_fn(roots)
        types_new = self.mode_type(modes_new)

        return ModeArray(modes_new, indices_new, types_new)

    def track_modes(self, initial_modes, callback, parameter_path, polarization="TE"):
        initial_modes = np.asarray(initial_modes)

        modearray_path = []

        modes = initial_modes

        for j, p in enumerate(parameter_path):
            callback(self.model, p)
            modearray_new = self.update_modes(modes, polarization)
            modearray_path.append(modearray_new)
            modes = modearray_new.modes

        # combine
        modes_combined = np.stack([ma.modes for ma in modearray_path], axis=0)
        indices_combined = np.stack([ma.indices for ma in modearray_path], axis=0)
        types_combined = np.stack([ma.types for ma in modearray_path], axis=0)

        return ModeArray(modes_combined, indices_combined, types_combined)

    def _radiation_modes(self, n_real_, n_clad_):
        n_real_ = np.asarray(n_real_)
        mask = n_real_ < np.real(n_clad_)
        n_imag = np.real(n_clad_) * np.imag(n_clad_) / n_real_

        modes = n_real_ + 1j * n_imag

        return ma.masked_array(modes, mask=~mask)

    def radiation_modes(self, n_real_):
        return (
            self._radiation_modes(n_real_, self.model.n_neg),
            self._radiation_modes(n_real_, self.model.n_pos),
        )

    def solution_pos_te(self, u, x0=0, normalized=True, force_resonant=False):
        return lambda x_: self.model.solution_pos_te(u, x_ - x0, normalized, force_resonant)

    def solution_pos_tm(self, u, x0=0, normalized=True, force_resonant=False):
        return lambda x_: self.model.solution_pos_tm(u, x_ - x0, normalized, force_resonant)

    def solution_neg_te(self, u, x0=0, normalized=True, force_resonant=False):
        return lambda x_: self.model.solution_neg_te(u, x_ - x0, normalized, force_resonant)

    def solution_neg_tm(self, u, x0=0, normalized=True, force_resonant=False):
        return lambda x_: self.model.solution_neg_tm(u, x_ - x0, normalized, force_resonant)

    #    def mode_type_fourier(self, q):
    #        p_neg = self.p_l_fn_p_parallel(q, self.n_neg)
    #        p_pos = self.p_l_fn_p_parallel(q, self.n_pos)
    #
    #        return self.mode_type_private(p_neg, p_pos)

    def mode_type(self, arg):
        arg = np.asarray(arg)

        def get_p_boundary(a):
            p_vec = self.model.p_vec_fn(a)
            return p_vec[0], p_vec[-1]

        p_neg, p_pos = np.vectorize(get_p_boundary)(arg)

        return self.mode_type_private(p_neg, p_pos)

    def mode_type_private(self, p_neg, p_pos):
        mask_outgoing_pos = np.real(p_pos) > 0
        mask_outgoing_neg = np.real(p_neg) > 0

        mask_decaying_pos = np.imag(p_pos) > 0
        mask_decaying_neg = np.imag(p_neg) > 0

        mask_outgoing = mask_outgoing_pos & mask_outgoing_neg
        mask_guided = mask_outgoing & mask_decaying_pos & mask_decaying_neg
        mask_leaky_neg = mask_outgoing & mask_decaying_pos & ~mask_decaying_neg
        mask_leaky_pos = mask_outgoing & ~mask_decaying_pos & mask_decaying_neg
        mask_leaky_double = mask_outgoing & ~mask_decaying_pos & ~mask_decaying_neg

        types = np.zeros(shape=p_neg.shape, dtype=np.int32)
        types[mask_guided] = ModeArray.GUIDED
        types[mask_leaky_neg] = ModeArray.LEAKY_NEG
        types[mask_leaky_pos] = ModeArray.LEAKY_POS
        types[mask_leaky_double] = ModeArray.LEAKY_DOUBLE

        return types


ModeResult = namedtuple("ModeResult", ["modes", "rootresult"])


class ModeArray(namedtuple("ModeArray", ["modes", "indices", "types"])):
    OTHER = 0
    GUIDED = 1
    LEAKY_NEG = 2
    LEAKY_POS = 3
    LEAKY_DOUBLE = 4

    @property
    def indices_guided(self):
        mask = self.types == self.GUIDED
        return ma.masked_array(self.indices, ~mask)

    @property
    def indices_leaky_pos(self):
        mask = self.types == self.LEAKY_POS
        return ma.masked_array(self.indices, ~mask)

    @property
    def indices_leaky_neg(self):
        mask = self.types == self.LEAKY_NEG
        return ma.masked_array(self.indices, ~mask)

    @property
    def indices_leaky_double(self):
        mask = self.types == self.LEAKY_DOUBLE
        return ma.masked_array(self.indices, ~mask)

    @property
    def indices_other(self):
        mask = self.types == self.OTHER
        return ma.masked_array(self.indices, ~mask)

    @property
    def modes_guided(self):
        mask = self.types == self.GUIDED
        return ma.masked_array(self.modes, ~mask)

    @property
    def modes_leaky_pos(self):
        mask = self.types == self.LEAKY_POS
        return ma.masked_array(self.modes, ~mask)

    @property
    def modes_leaky_neg(self):
        mask = self.types == self.LEAKY_NEG
        return ma.masked_array(self.modes, ~mask)

    @property
    def modes_leaky_double(self):
        mask = self.types == self.LEAKY_DOUBLE
        return ma.masked_array(self.modes, ~mask)

    @property
    def modes_other(self):
        mask = self.types == self.OTHER
        return ma.masked_array(self.modes, ~mask)

    def __getitem__(self, key):
        return ModeArray(self.modes[key], self.indices[key], self.types[key])

    def _add(first, second):
        if not isinstance(first, ModeArray) or not isinstance(second, ModeArray):
            raise TypeError("Must be ModeArray")
        return ModeArray(
            np.concatenate([first.modes, second.modes]),
            np.concatenate([first.indices, second.indices]),
            np.concatenate([first.types, second.types]),
        )

    def __add__(self, other):
        return ModeArray._add(self, other)

    def __radd__(self, other):
        return ModeArray._add(other, self)


def get_contours(branchpoints, x_range=(1 - 5e-5, 1), y_range=(0, 1e-5), eps=1e-7):
    branchpoints = sorted(branchpoints, key=lambda x: x.real)

    xleft = [x_range[0]] + [(p.real + eps) for p in branchpoints]
    xright = [(p.real - eps) for p in branchpoints] + [x_range[1]]
    xranges = filter(lambda x: (x[1] - x[0]) > eps, zip(xleft, xright))
    contours = [cxroots.Rectangle(x_range=xrange, y_range=y_range) for xrange in xranges]

    return contours
