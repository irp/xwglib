#undef NDEBUG
#undef EIGEN_NO_DEBUG

#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>

#include "model.h"

namespace py = pybind11;
namespace s = model;

PYBIND11_MODULE(_tmm, m) {
  py::class_<s::StackData>(m, "StackData")
      .def(py::init<const s::a_real &, const s::a_complex &,
                    const s::a_complex &, const s::real>())
      .def_readwrite("n", &s::StackData::n)
      .def_readwrite("d", &s::StackData::d)
      .def_readwrite("sigma", &s::StackData::sigma)
      .def_readwrite("k_vac", &s::StackData::k_vac);

  py::class_<s::Stack>(m, "Stack")
      .def(py::init<const s::StackData &, const bool, const s::BranchCut>())
      .def("wronskian_te", &s::Stack::wronskian_te)
      .def("wronskian_tm", &s::Stack::wronskian_tm)
      .def("diff_wronskian_te", &s::Stack::diff_wronskian_te)
      .def("diff_wronskian_tm", &s::Stack::diff_wronskian_tm)
      .def("greens_function_te", py::vectorize(&s::Stack::greens_function_te))
      .def("greens_function_tm", py::vectorize(&s::Stack::greens_function_tm))
      .def("reflectivity_te", py::vectorize(&s::Stack::reflectivity_te))
      .def("reflectivity_tm", py::vectorize(&s::Stack::reflectivity_tm))
      .def("residue_te", py::vectorize(&s::Stack::residue_te))
      .def("residue_tm", py::vectorize(&s::Stack::residue_tm))
      .def("coefficients_pos", &s::Stack::coefficients_pos)
      .def("coefficients_neg", &s::Stack::coefficients_neg)
      .def("solution_pos_te", py::vectorize(&s::Stack::solution_pos_te))
      .def("solution_pos_tm", py::vectorize(&s::Stack::solution_pos_tm))
      .def("solution_neg_te", py::vectorize(&s::Stack::solution_neg_te))
      .def("solution_neg_tm", py::vectorize(&s::Stack::solution_neg_tm))
      .def("u_fn", &s::Stack::u_fn)
      .def("mode_index", &s::Stack::mode_index)
      .def("index_profile", py::vectorize(&s::Stack::index_profile))
      .def_property("data", &s::Stack::get_data, &s::Stack::set_data,
                    py::return_value_policy::copy)
      .def("p_vec_fn", &s::Stack::p_vec_fn)
      .def("p_l_fn", &s::Stack::p_l_fn);

  py::enum_<s::BranchCut>(m, "BranchCut")
      .value("vertical", s::BranchCut::vertical)
      .value("decaying", s::BranchCut::decaying)
      .value("outgoing", s::BranchCut::outgoing)
      .export_values();
}
