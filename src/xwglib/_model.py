import numpy as np

from ._tmm import BranchCut, Stack, StackData


class Model(Stack):
    def __init__(self, d, n, mu, k_vac, method="fourier", branch=BranchCut.decaying):
        if method not in {"fourier", "conformal"}:
            raise ValueError

        if method == "conformal":
            self.conformal = True
        else:
            self.conformal = False

        d = np.asarray(d, dtype=np.float64)
        n = np.asarray(n, dtype=np.complex128)
        mu = np.asarray(mu) * np.ones_like(n)
        eps = n**2 / mu
        sd = StackData(d, eps, mu, k_vac)

        super().__init__(sd, self.conformal, branch)

    def _get_sigma(self):
        return self.data.sigma

    def _set_sigma(self, value):
        value = np.asarray(value)
        data = self.data
        if value.shape != data.sigma.shape:
            raise ValueError
        data.sigma = value
        self.data = data

    sigma = property(fget=_get_sigma, fset=_set_sigma)

    def _get_d(self):
        return self.data.d

    def _set_d(self, value):
        value = np.asarray(value)
        data = self.data
        if value.shape != data.d.shape:
            raise ValueError
        data.d = value
        self.data = data

    d = property(fget=_get_d, fset=_set_d)

    def _get_n(self):
        return self.data.n

    def _set_n(self, value):
        value = np.asarray(value)
        data = self.data
        if value.shape != data.n.shape:
            raise ValueError
        data.n = value
        self.data = data

    n = property(fget=_get_n, fset=_set_n)

    @property
    def n_neg(self):
        return self.data.n[0]

    @property
    def n_pos(self):
        return self.data.n[-1]

    @property
    def wavelength(self):
        return 2 * np.pi / self.data.k_vac
