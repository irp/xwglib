variables:
  # these are automatically set as environment variables
  MESON_PACKAGE_CACHE_DIR: "$CI_PROJECT_DIR/.cache/meson-package"
  UV_VERSION: 0.5
  PYTHON_VERSION: 3.12
  BASE_LAYER: bookworm-slim
  WHEEL_IMAGE: quay.io/pypa/manylinux_2_28_x86_64

default:
  image: ghcr.io/astral-sh/uv:$UV_VERSION-python$PYTHON_VERSION-$BASE_LAYER
  #  cache:
  #  paths:
  #    - .cache/meson-package

.setup_environment:
  before_script:
    - curl -L https://micromamba.snakepit.net/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
    - eval "$(./bin/micromamba shell hook -s posix)"
    - micromamba create --yes -f devenv.yml
    - micromamba activate xwglib


stages:
  - lint
  - build
  - test
  - deploy

## 1 - lint

ruff-check:
  stage: lint
  script:
    - uv tool run ruff check -o ruff-check.log src/
  artifacts:
    when: on_failure
    paths:
      - ruff-check.log
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"


ruff-format:
  stage: lint
  script:
    - uv tool run ruff format --check src/
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"


clang-format:
  stage: lint
  variables:
    CPP_FILES_LIST: /tmp/clang-format-files
  script:
    - find include -regex '.*\.\(cpp\|h\)' > ${CPP_FILES_LIST}
    - find src -regex '.*\.\(cpp\|h\)' >> ${CPP_FILES_LIST}
    - uv tool run clang-format --style=Google --dry-run -i --files=${CPP_FILES_LIST}
  allow_failure: true
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

# 2 - build

build_in_condaenv:
  image: python:3.12
  stage: build
  extends: .setup_environment
  script:
    - meson setup build
    - meson compile -Cbuild
    - meson test -Cbuild
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

build_whl_linux:
  stage: build
  image: ${WHEEL_IMAGE}
  variables:
    BW_OUTDIR: wheelhouse
    BW_EXTRA_BUILD_FLAGS: --installer=uv
    BW_PYVERS: cp310-cp310 cp311-cp311 cp312-cp312 cp313-cp313
  script:
    - bash ci/build_wheels.sh
  artifacts:
    paths:
      - wheelhouse/
  rules:
    # run only if tag is created
    - if: $CI_COMMIT_TAG =~ /^v\S+/
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual

build_conda_linux:
  stage: build
  #  image: condaforge/miniforge3
  image: mambaorg/micromamba
  script:
    - micromamba install -c conda-forge rattler-build -y --quiet
    - rattler-build build --experimental --recipe conda/recipe.yaml --output-dir /tmp/rattler/
    - mkdir conda-dist
    - cp -r /tmp/rattler/linux-64 conda-dist/
  artifacts:
    paths:
      - conda-dist/linux-64/*
  rules:
    # run only if tag is created
    - if: $CI_COMMIT_TAG =~ /^v\S+/
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual

# 4 - deploy

upload_whl:
  stage: deploy
  dependencies:
    - build_whl_linux
  script:
    - uv publish --username=gitlab-ci-token --password=${CI_JOB_TOKEN} --publish-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi wheelhouse/* 
  rules:
    # run only if tag in format 'vXXX' is created
    - if: $CI_COMMIT_TAG =~ /^v\S+/
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
