# xwglib -- X-ray waveguide library 

## Installation 

We provide compiled "wheels" for Linux (distribution-agnostic manylinux standard) that bundle all non-python dependencies. They can be installed via 
```
pip install xwglib --index-url https://gitlab.gwdg.de/api/v4/projects/18955/packages/pypi/simple
```

## Development

### Requirements

Building and running the package requires a C++ compiler and the C++ libraries 
 - nanobind
 - eigen >= 3.4
 - catch2

The libraries can either be provided externally (managed through conda or the system package manager) or will automatically be fetched through Meson's wrap system.

The build system itself will be fetched from PyPI when using PEP 517 build isolation. Otherwise, the following packages need to be provided
 - meson
 - meson-python
 - ninja
 - cmake
 - pkg-config

The C++ libraries use some features of C++20 and require a up-to-date compiler.
Minimum version for GCC: GCC 10.1.


### Conda development environment
The recommended way of managing the dependencies is through conda.
A conda environment file `environment.yml` that sets up an appropriate conda environment to build and run has been provided.

To setup the environment, run
```
git clone git@gitlab.gwdg.de:irp/xwglib.git xwglib-git
cd xwglib-git
conda env create -f devenv.yml
conda activate xwglib
```

### Installation with meson-python through PEP 517
To build and install the package from the activate conda environment, run
```
python -m build -wnx .
pip install dist/*whl
```

The same can be achieved in the single step without building the wheel separately
```
pip install --no-build-isolation --no-deps .
```
Editable installation is also supported by adding the `-e` flag.

### Installation with Meson 
Meson can also be used directly without PEP 517. Note that installation through this method does not produce the metadata for the installed package to be recognized by pip.
Meson needs to be given the correct paths for the conda environment, as well as the conda installation of Boost. To do this, setup the build directory as follows
```
conda activate xwglib
meson setup --prefix=$CONDA_PREFIX --cmake-prefix-path=$CONDA_PREFIX build
```
After this, the package can be compiled, tested, and installed via
```
meson compile -Cbuild
meson test -Cbuild
meson install -Cbuild
```

### Build conda package
Conda packages can be build with `rattler-build`.
Install rattler-build with conda/mamba/micromamba
```
mamba install rattler-build -c conda-forge
```

Conda packages can then be build via 
```
rattler-build build --experimental --recipe conda/recipe.yaml --output-dir /tmp/rattler/
mkdir conda-dist
cp -r /tmp/rattler/linux-64 conda-dist/
```
where `linux-64` must be replaced with the appropriate platform.
IMPORTANT: the output-dir argument must point to a place *outside* the source tree due to current limitations of rattler-build.


## Usage

See the [examples](./examples/).
