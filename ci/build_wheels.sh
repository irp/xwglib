#!/bin/bash
# MUST BE RUN ON WITH
#image: quay.io/pypa/manylinux_2_28_x86_64

set -e -u -x

BW_PROJECT_DIR=$(pwd)
BW_TMP_DIR="/tmp/cibuildwheel/"

export PIP_DISABLE_PIP_VERSION_CHECK=1
export PIP_ROOT_USER_ACTION="ignore"

mkdir $BW_OUTDIR

for pyver in $BW_PYVERS; do

PYTHON=/opt/python/$pyver/bin/python
build_wheel_dir="$BW_TMP_DIR/$pyver/built_wheel"
repaired_wheel_dir="$BW_TMP_DIR/$pyver/repaired_wheel"

mkdir -p $build_wheel_dir
mkdir -p $repaired_wheel_dir

echo "Building wheel for $pyver..."
${PYTHON} -m build ${BW_PROJECT_DIR} --wheel --outdir=${build_wheel_dir} ${BW_EXTRA_BUILD_FLAGS}
build_wheel="${build_wheel_dir}/*.whl"

echo "Repairing wheel $build_wheel..."
auditwheel repair -w ${repaired_wheel_dir} ${build_wheel}
repaired_wheel="${repaired_wheel_dir}/*.whl"

## TODO: run tests in env

cp $repaired_wheel $BW_OUTDIR/

done
