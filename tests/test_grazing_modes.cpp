#include <catch2/catch.hpp>
#include <cmath>

#include "model.h"

using namespace model;
using namespace std::complex_literals;

TEST_CASE("Fourier space Green's function agrees with Mathematica: Pt/Air cap, z dependence",
          "[mathematica]")
{
  constexpr double k0 = 73.026;
  // constexpr double z0 = 12;
  constexpr double q0 = 0.9999500004166653;
  a_real d(5);
  d << 1.5, 10, 1, 15, 1.5;
  a_complex eps(7);
  eps << 0.9999839600291892 + 2.565105665264699e-6i,
      0.9999839600291892 + 2.565105665264699e-6i,
      0.9999976274889923 + 1.0400838803358954e-9i,
      0.9999925761232781 + 3.4334873033095585e-7i,
      0.9999976274889923 + 1.0400838803358954e-9i,
      0.9999839600291892 + 2.565105665264699e-6i, 1.;
  eps = eps.unaryExpr([](complex x)
                      { return x * x; });

  a_complex mu = a_complex::Ones(7);
  a_real z(10);
  z << 0.,
      11.11111111111111,
      22.22222222222222,
      33.333333333333336,
      44.44444444444444,
      55.55555555555556,
      66.66666666666667,
      77.77777777777777,
      88.88888888888889,
      100.;
  a_complex result_mma(10);
  result_mma << 0.06392591448397299 + 0.9127659433905476i, -0.6515788984082727 + 0.135455496234377i, -0.046093711679581276 - 0.8314835833641755i, 0.6928935267542444 - 0.07764285983287857i, -0.10301555158157752 + 0.6895778775695363i, -0.6399502147104422 - 0.2767554440169546i, 0.43190845551446194 - 0.5473435293817543i, 0.4179772816572227 + 0.5580545180530316i, -0.6467216687648305 + 0.26053970187093556i, -0.08560428952903472 - 0.691955026511978i;

  StackData sd {d, eps, mu, k0};
  Stack stack(sd, false, BranchCut::vertical);
  a_complex result = z.unaryExpr([=, &stack](real _z)
                                 { return stack.greens_function_te(0, _z, q0); });
  for (Eigen::Index j = 0; j < z.size(); j++)
  {

    INFO("z is " << z(j));
    CHECK(result(j).real() == Approx(result_mma(j).real()).epsilon(1e-4));
    CHECK(result(j).imag() == Approx(result_mma(j).imag()).epsilon(1e-4));
  }
}
