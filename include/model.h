#pragma once

#include <Eigen/Dense>
#include <complex>
#include <numeric>
#include <numbers>

namespace model {

using real = double;
using complex = std::complex<real>;
using a_real = Eigen::Array<real, Eigen::Dynamic, 1>;
using a_complex = Eigen::Array<complex, Eigen::Dynamic, 1>;
using a2n_complex = Eigen::Array<complex, 2, Eigen::Dynamic>;

using mat22 = Eigen::Matrix<complex, 2, 2>;
using vec2 = Eigen::Matrix<complex, 2, 1>;

template <typename T>
inline T squared(const T in) {
  return in * in;
}

enum class BranchCut {
  decaying,
  outgoing,
  vertical
};

struct StackData {
  a_real d;        // inner layer thicknesses
  a_complex n;     // refractive indices
  a_complex eps;   // relative permittivity
  a_complex mu;    // relative permeability
  a_real sigma;    // interfacial rms roughness
  real k_vac;      // vacuum wavenumber
  int num_layers;  // number of layers, including outer layers

  StackData(const a_real &d, const a_complex &eps, const a_complex &mu,
            const a_real &sigma, const real k_vac)
      : d{d},
        n{(eps * mu).sqrt()},
        eps{eps},
        mu{mu},
        sigma{sigma},
        k_vac{k_vac},
        num_layers{static_cast<int>(d.size()) + 2} {
    if (eps.size() != num_layers) {
      throw std::runtime_error("eps size mismatch");
    }
    if (mu.size() != num_layers) {
      throw std::runtime_error("mu size mismatch");
    }
    if (sigma.size() != num_layers - 1) {
      throw std::runtime_error("sigma size mismatch");
    }
  };

  StackData(const a_real &d, const a_complex &eps, const a_complex &mu,
            const real k_vac)
      : d{d},
        n{(eps * mu).sqrt()},
        eps{eps},
        mu{mu},
        sigma{a_real::Zero(d.size() + 1)},
        k_vac{k_vac},
        num_layers{static_cast<int>(d.size()) + 2} {
    if (eps.size() != num_layers) {
      throw std::runtime_error("eps size mismatch");
    }
    if (mu.size() != num_layers) {
      throw std::runtime_error("mu size mismatch");
    }
  };

  StackData &operator=(const StackData &other) = default;
};

class PImplFourier {
 public:
  const StackData &data;
  const BranchCut branch;

  PImplFourier(const StackData &stackdata, const BranchCut branch)
      : data{stackdata}, branch{branch} {};

  [[nodiscard]] inline complex p_l_fn(const complex q,
                                      const complex n_l) const {
    using namespace std::complex_literals;

    switch (branch) {
      case BranchCut::vertical:
        return 1.0i * std::sqrt(-1.0i * (n_l - q)) *
               std::sqrt(-1.0i * (n_l + q));
      case BranchCut::outgoing:
        return std::sqrt(-squared(q) + squared(n_l));

      default:  // case BranchCut::decaying:
        return 1.0i * std::sqrt(squared(q) - squared(n_l));
    }
  }

  [[nodiscard]] a_complex p_vec_fn(const complex q) const {
    a_complex p_vec(data.num_layers);
    p_vec = data.n.unaryExpr([&](complex n_l) { return p_l_fn(q, n_l); });
    return p_vec;
  }

  [[nodiscard]] a_complex diff_p_vec_fn(const complex q) const {
    a_complex diff_p_vec(data.num_layers);
    diff_p_vec = data.n.unaryExpr([&](complex n_l) {
      return -1.0 * q / p_l_fn(q, n_l);
    });
    return diff_p_vec;
  }

  [[nodiscard]] a_complex mode_index(const a_complex q) const { return q; }

  [[nodiscard]] a_complex u_fn(const a_complex q) const {
    using namespace std::complex_literals;

    auto p_pos =
        q.unaryExpr([&](complex p) { return p_l_fn(p, data.n(Eigen::last)); });
    auto p_neg = q.unaryExpr([&](complex p) { return p_l_fn(p, data.n[0]); });

    a_complex u = 0.5 * (p_pos + p_neg);

    return u;
  }
};

class PImplConformal {
 private:
  template <typename T>
  [[nodiscard]] inline T p_neg_fn(const T u) const {
    return u - 0.25 / u * (squared(data.n(Eigen::last)) - squared(data.n[0]));
  }

  template <typename T>
  [[nodiscard]] inline T p_pos_fn(const T u) const {
    return u + 0.25 / u * (squared(data.n(Eigen::last)) - squared(data.n[0]));
  }

  [[nodiscard]] inline complex diff_p_pos_fn(const complex u) const {
    return 1.0 - 0.25 / squared(u) *
                     (squared(data.n(Eigen::last)) - squared(data.n[0]));
  }

  [[nodiscard]] inline complex diff_p_neg_fn(const complex u) const {
    return 1.0 + 0.25 / squared(u) *
                     (squared(data.n(Eigen::last)) - squared(data.n[0]));
  }

 public:
  const StackData &data;

  PImplConformal(const StackData &stackdata) : data{stackdata} {};

  [[nodiscard]] inline complex p_l_fn(const complex p_pos,
                                      const complex n_l) const {
    using namespace std::complex_literals;
    return 1.0i * std::sqrt(-squared(p_pos) -
                            (squared(n_l) - squared(data.n(Eigen::last))));
  }

  [[nodiscard]] inline complex diff_p_l_fn(const complex p_pos,
                                           const complex n_l) const {
    using namespace std::complex_literals;
    return -1.0i * p_pos /
           std::sqrt(-squared(p_pos) -
                     (squared(n_l) - squared(data.n(Eigen::last))));
  }

  [[nodiscard]] a_complex p_vec_fn(const complex u) const {
    a_complex p_vec(data.num_layers);
    const complex p_neg = p_neg_fn(u);
    const complex p_pos = p_pos_fn(u);
    p_vec[0] = p_neg;
    p_vec(data.num_layers - 1) = p_pos;
    p_vec.segment(1, data.num_layers - 2) =
        data.n.segment(1, data.num_layers - 2)
            .unaryExpr([&](complex n_l) { return p_l_fn(p_pos, n_l); });
    return p_vec;
  }

  [[nodiscard]] a_complex diff_p_vec_fn(const complex u) const {
    a_complex diff_p_vec(data.num_layers);
    const complex p_pos = p_pos_fn(u);
    const complex diff_p_neg = diff_p_neg_fn(u);
    const complex diff_p_pos = diff_p_pos_fn(u);
    diff_p_vec[0] = diff_p_neg;
    diff_p_vec(data.num_layers - 1) = diff_p_pos;
    diff_p_vec.segment(1, data.num_layers - 2) =
        data.n.segment(1, data.num_layers - 2).unaryExpr([&](complex n_l) {
          return diff_p_l_fn(p_pos, n_l) * diff_p_pos;
        });
    return diff_p_vec;
  }

  [[nodiscard]] a_complex mode_index(const a_complex u) const {
    a_complex p_pos = p_pos_fn<a_complex>(u);

    return (squared(data.n(Eigen::last)) - squared(p_pos)).sqrt();
  }

  [[nodiscard]] a_complex u_fn(const a_complex u) const { return u; }
};

class Stack {
 public:
  StackData data;
  const bool conformal;
  const PImplFourier impl_fourier;      // implementation
  const PImplConformal impl_conformal;  // implementation

  Stack(const StackData &stackdata, const bool conformal,
        const BranchCut branch)
      : data{stackdata},
        conformal{conformal},
        impl_fourier{data, branch},
        impl_conformal{data} {};

  [[nodiscard]] a_complex p_vec_fn(const complex arg) const {
    if (conformal) {
      return impl_conformal.p_vec_fn(arg);
    } else {
      return impl_fourier.p_vec_fn(arg);
    }
  }

  [[nodiscard]] inline complex p_l_fn(const complex arg,
                                      const complex n_l) const {
    if (conformal) {
      return impl_conformal.p_l_fn(arg, n_l);
    } else {
      return impl_fourier.p_l_fn(arg, n_l);
    }          
  }
  
  [[nodiscard]] a_complex diff_p_vec_fn(const complex arg) const {
    if (conformal) {
      return impl_conformal.diff_p_vec_fn(arg);
    } else {
      return impl_fourier.diff_p_vec_fn(arg);
    }
  }

  [[nodiscard]] a_complex u_fn(const a_complex arg) const {
    if (conformal) {
      return impl_conformal.u_fn(arg);
    } else {
      return impl_fourier.u_fn(arg);
    }
  }

  [[nodiscard]] a_complex mode_index(const a_complex arg) const {
    if (conformal) {
      return impl_conformal.mode_index(arg);
    } else {
      return impl_fourier.mode_index(arg);
    }
  }

  [[nodiscard]] complex angle_to_arg(const real angle) const {
    complex q = data.n[0] * std::cos(angle);
    if (conformal) {
      a_complex a_q{{q}};
      return impl_fourier.u_fn(a_q)[0];
    } else {
      return q;
    }
  }

  [[nodiscard]] inline mat22 transfer_l_fn(const complex p_l, const real d_l,
                                           const complex gamma_l) const {
    const complex phi_l = d_l * data.k_vac * p_l;

    mat22 m{{std::cos(phi_l), gamma_l / p_l * std::sin(phi_l)},
            {-p_l / gamma_l *std::sin(phi_l), std::cos(phi_l)}, };
    return m;
  }

  [[nodiscard]] inline mat22 diff_transfer_l_fn(const complex p_l,
                                                const complex diff_p_l,
                                                const real d_l,
                                                const complex gamma_l) const {
    const complex phi_l = d_l * data.k_vac * p_l;

    mat22 m{{-data.k_vac *d_l *diff_p_l *std::sin(phi_l),
             (diff_p_l * gamma_l / p_l) * (-1.0 / p_l * std::sin(phi_l) +
                                           data.k_vac * d_l * std::cos(phi_l))},
            {-diff_p_l / gamma_l *(std::sin(phi_l) +
                                   p_l * data.k_vac * d_l * std::cos(phi_l)),
             -data.k_vac * d_l * diff_p_l * std::sin(phi_l)}};

    return m;
  }

  [[nodiscard]] mat22 roughness_matrix_l(const complex p_l, const complex p_lp,
                                         const complex gamma_l,
                                         const complex gamma_lp,
                                         const complex sigma) const {
    const complex c = cosh(p_l * p_lp * squared(data.k_vac) * squared(sigma));
    const complex s = sinh(p_l * p_lp * squared(data.k_vac) * squared(sigma));
    const complex e = exp(-0.5 * squared(data.k_vac) *
                          (squared(p_l) + squared(p_lp)) * squared(sigma));

    const complex m11 = e * (c + p_l / p_lp * gamma_lp / gamma_l * s);
    const complex m22 = e * (c + p_lp / p_l * gamma_l / gamma_lp * s);

    mat22 m{{m11, 0}, {0, m22}};

    return m;
  }

  [[nodiscard]] mat22 diff_roughness_matrix_l(const complex p_l,
                                              const complex p_lp,
                                              const complex diff_p_l,
                                              const complex diff_p_lp,
                                              const complex gamma_l,
                                              const complex gamma_lp,
                                              const complex sigma) const {

    const complex c = cosh(p_l * p_lp * squared(data.k_vac) * squared(sigma));
    const complex s = sinh(p_l * p_lp * squared(data.k_vac) * squared(sigma));
    const complex e = exp(-0.5 * squared(data.k_vac) *
                          (squared(p_l) + squared(p_lp)) * squared(sigma));

    const complex diff_c = squared(data.k_vac) *
                           (diff_p_lp * p_l + diff_p_l * p_lp) *
                           squared(sigma) * s;
    const complex diff_s = squared(data.k_vac) *
                           (diff_p_lp * p_l + diff_p_l * p_lp) *
                           squared(sigma) * c;
    const complex diff_e = -squared(data.k_vac) * squared(sigma) *
                           (diff_p_l * p_l + diff_p_lp * p_lp) * e;

    const complex m11 =
        diff_e * (c + p_l / p_lp * gamma_lp / gamma_l * s) +
        e * (diff_c + p_l / p_lp * gamma_lp / gamma_l * diff_s) +
        e * (diff_p_l / p_lp - p_l * diff_p_lp / squared(p_lp)) *
            (gamma_lp / gamma_l * s);

    const complex m22 =
        diff_e * (c + p_lp / p_l * gamma_l / gamma_lp * s) +
        e * (diff_c + p_lp / p_l * gamma_l / gamma_lp * diff_s) +
        e * (diff_p_lp / p_l - p_lp * diff_p_l / squared(p_l)) *
            (gamma_l / gamma_lp * s);

    mat22 m{{m11, 0}, {0, m22}};

    return m;
  }

  [[nodiscard]] inline mat22 transfer_l_combined(const complex p_l,
                                                 const complex p_lp,
                                                 const complex gamma_l,
                                                 const complex gamma_lp,
                                                 const real d_l,
                                                 const real sigma_l) const {
    mat22 m_l{transfer_l_fn(p_l, d_l, gamma_l)};
    mat22 r_l{roughness_matrix_l(p_l, p_lp, gamma_l, gamma_lp, sigma_l)};

    mat22 ret = r_l * m_l;

    return ret;
  }

  [[nodiscard]] inline mat22 diff_transfer_l_combined(
                    const complex p_l, const complex p_lp,
                    const complex diff_p_l, const complex diff_p_lp,
                    const complex gamma_l, const complex gamma_lp,
                    const real d_l, const real sigma_l) const {
    mat22 m_l{transfer_l_fn(p_l, d_l, gamma_l)};
    mat22 diff_m_l{diff_transfer_l_fn(p_l, diff_p_l, d_l, gamma_l)};
    mat22 r_l{roughness_matrix_l(p_l, p_lp, gamma_l, gamma_lp, sigma_l)};
    mat22 diff_r_l{diff_roughness_matrix_l(p_l, p_lp, diff_p_l, diff_p_lp,
                                           gamma_l, gamma_lp, sigma_l)};

    mat22 ret = diff_r_l * m_l + r_l * diff_m_l;

    return ret;
  }

  [[nodiscard]] mat22 transfer_full_fn(const a_complex &p_vec,
                                       const a_complex &gamma) const {
    mat22 m{mat22::Identity()};
    a_real d = a_real::Zero(data.d.size() + 1);
    d.tail(data.d.size()) = data.d;

    for (int j = 0; j < data.num_layers - 1; j++) {
      const mat22 m_j{transfer_l_combined(p_vec[j], p_vec[j + 1], gamma[j],
                                          gamma[j + 1], d[j], data.sigma[j])};
      m = m_j * m;
    }

    return m;
  }

  [[nodiscard]] mat22 diff_transfer_full_fn(const a_complex &p_vec,
                                            const a_complex &diff_p_vec,
                                            const a_complex &gamma) const {
    mat22 m{mat22::Zero()};
    a_real d = a_real::Zero(data.d.size() + 1);
    d.tail(data.d.size()) = data.d;

    for (int l = 0; l < data.num_layers - 1; l++) {
      mat22 m_tmp{mat22::Identity()};

      for (int j = 0; j < l; j++) {
        const mat22 mj = transfer_l_combined(p_vec[j], p_vec[j + 1], gamma[j],
                                             gamma[j + 1], d[j], data.sigma[j]);
        m_tmp = mj * m_tmp;
      }
      const mat22 diff_m_l = {diff_transfer_l_combined(
          p_vec[l], p_vec[l + 1], diff_p_vec[l], diff_p_vec[l + 1], gamma[l],
          gamma[l + 1], d[l], data.sigma[l])};
      m_tmp = diff_m_l * m_tmp;
      for (int j = l + 1; j < data.num_layers - 1; j++) {
        const mat22 mj = transfer_l_combined(p_vec[j], p_vec[j + 1], gamma[j],
                                             gamma[j + 1], d[j], data.sigma[j]);
        m_tmp = mj * m_tmp;
      }

      m += m_tmp;
    }

    return m;
  }

  [[nodiscard]] static mat22 cob_l_fn(const complex p_l,
                                      const complex gamma_l) {
    using namespace std::complex_literals;

    mat22 m{{1, 1, }, {1.0i * p_l / gamma_l, -1.0i * p_l / gamma_l}};
    return m;
  }

  [[nodiscard]] static mat22 cob_l_inverse_fn(const complex p_l,
                                              const complex gamma_l) {
    using namespace std::complex_literals;

    mat22 m{{0.5, -0.5i * gamma_l / p_l}, {0.5, 0.5i * gamma_l / p_l}};
    return m;
  }

  [[nodiscard]] complex wronskian_te(const complex u) const {
    const a_complex p_vec = p_vec_fn(u);
    return wronskian_fn_private(p_vec, data.mu);
  }

  [[nodiscard]] complex wronskian_tm(const complex u) const {
    const a_complex p_vec = p_vec_fn(u);
    return wronskian_fn_private(p_vec, data.eps);
  }

  [[nodiscard]] complex wronskian_fn_private(const a_complex &p_vec,
                                             const a_complex &gamma) const {
    using namespace std::complex_literals;

    const complex p_neg = p_vec[0];
    const complex p_pos = p_vec(Eigen::last);
    const complex gamma_neg = gamma[0];
    const complex gamma_pos = gamma(Eigen::last);

    const mat22 m{transfer_full_fn(p_vec, gamma)};

    const vec2 right{1.0, -1.0i * p_neg / gamma_neg};  // substrate (negative
                                                       // z): negative-going
                                                       // wave
    const vec2 left{1.0i * p_pos / gamma_pos,
                    -1};  // superstrate (positive z): positive-going wave

    const complex ret = (left.transpose() * m * right).value();

    return ret;
  }

  [[nodiscard]] complex reflectivity_te(const real theta) {
    const complex arg = angle_to_arg(theta);
    const a_complex p_vec = p_vec_fn(arg);

    return reflectivity_private(p_vec, data.mu);
  }

  [[nodiscard]] complex reflectivity_tm(const real theta) {
    const complex arg = angle_to_arg(theta);
    const a_complex p_vec = p_vec_fn(arg);

    return reflectivity_private(p_vec, data.eps);
  }

  [[nodiscard]] complex reflectivity_private(const a_complex &p_vec,
                                             const a_complex &gamma) {

    using namespace std::complex_literals;

    const complex p_neg = p_vec[0];
    const complex p_pos = p_vec(Eigen::last);
    const complex gamma_neg = gamma[0];
    const complex gamma_pos = gamma(Eigen::last);

    const mat22 m{transfer_full_fn(p_vec, gamma)};

    const vec2 right{1, 1.0i * p_pos / gamma_pos};

    const mat22 cob_neg = cob_l_inverse_fn(p_neg, gamma_neg);

    const vec2 left = cob_neg * m.inverse() * right;
    const complex ret = left(1) / left(0);

    return ret;
  }

  [[nodiscard]] complex diff_wronskian_te(const complex u) const {
    const a_complex p_vec = p_vec_fn(u);
    const a_complex diff_p_vec = diff_p_vec_fn(u);
    return diff_wronskian_fn_private(p_vec, diff_p_vec, data.mu);
  }

  [[nodiscard]] complex diff_wronskian_tm(const complex u) const {
    const a_complex p_vec = p_vec_fn(u);
    const a_complex diff_p_vec = diff_p_vec_fn(u);
    return diff_wronskian_fn_private(p_vec, diff_p_vec, data.eps);
  }

  [[nodiscard]] complex diff_wronskian_fn_private(
                    const a_complex &p_vec, const a_complex &diff_p_vec,
                    const a_complex &gamma) const {
    using namespace std::complex_literals;

    const complex p_neg = p_vec[0];
    const complex p_pos = p_vec(Eigen::last);

    const complex diff_p_neg = diff_p_vec[0];
    const complex diff_p_pos = diff_p_vec(Eigen::last);

    const complex gamma_neg = gamma[0];
    const complex gamma_pos = gamma(Eigen::last);

    const mat22 m{transfer_full_fn(p_vec, gamma)};
    const mat22 diff_m{diff_transfer_full_fn(p_vec, diff_p_vec, gamma)};

    const vec2 right{1.0, -1.0i * p_neg *gamma_neg};  // substrate (negative z):
                                                      // negative-going wave
    const vec2 left{1.0i * p_pos *gamma_pos,
                    -1};  // superstrate (positive z): positive-going wave

    const vec2 diff_right{0, -1.0i * diff_p_neg *gamma_neg};
    const vec2 diff_left{1.0i * diff_p_pos *gamma_pos, 0};

    const complex ret =
        (diff_left.transpose() * m * right + left.transpose() * diff_m * right +
         left.transpose() * m * diff_right).value();

    return ret;
  }

  [[nodiscard]] a2n_complex coefficients_neg(const a_complex &p_vec,
                                             const a_complex &gamma,
                                             const bool force_guided) const {
    a2n_complex coefficients(2, data.num_layers);

    const complex p_neg = p_vec[0];
    const complex gamma_neg = gamma[0];

    const vec2 right{0, 1.0};

    const mat22 a0{cob_l_fn(p_neg, gamma_neg)};
    coefficients.col(0) = right;
    vec2 tmp = a0 * right;

    // "virtual" first layer has thickness 0 -> identity matrix
    const mat22 cob_1{cob_l_inverse_fn(p_vec[1], gamma[1])};
    coefficients.col(1) = cob_1 * tmp;

    for (int l = 1; l <= data.num_layers - 2; l++) {
      const mat22 m_l = transfer_l_fn(p_vec[l], data.d[l - 1], gamma[l]);

      tmp = m_l * tmp;

      const mat22 cob_l{cob_l_inverse_fn(p_vec[l + 1], gamma[l + 1])};
      coefficients.col(l + 1) = cob_l * tmp;
    }

    if (force_guided) {
      coefficients(1, data.num_layers - 1) = 0.0;
    }

    return coefficients;
  }

  [[nodiscard]] a2n_complex coefficients_pos(const a_complex &p_vec,
                                             const a_complex &gamma,
                                             const bool force_guided) const {
    a2n_complex coefficients(2, data.num_layers);

    const complex p_pos = p_vec(Eigen::last);
    const complex gamma_pos = gamma(Eigen::last);

    const vec2 right{1.0, 0};

    const mat22 aend{cob_l_fn(p_pos, gamma_pos)};
    coefficients.col(data.num_layers - 1) = right;
    vec2 tmp = aend * right;

    for (int l = data.num_layers - 2; l >= 1; l--) {
      const mat22 m_l = transfer_l_fn(p_vec[l], -data.d[l - 1], gamma[l]);

      tmp = m_l * tmp;

      const mat22 cob_l{cob_l_inverse_fn(p_vec[l], gamma[l])};
      coefficients.col(l) = cob_l * tmp;
    }

    // "virtual" first layer has thickness 0 -> identity matrix
    const mat22 cob_0{cob_l_inverse_fn(p_vec[0], gamma[0])};
    coefficients.col(0) = cob_0 * tmp;

    if (force_guided) {
      coefficients(0, 0) = 0.0;
    }

    return coefficients;
  }

  [[nodiscard]] a_real interfaces(const real z0) const {
    a_real d_ext(data.num_layers - 1);
    d_ext[0] = z0;
    d_ext.tail(data.num_layers - 2) = data.d;
    a_real z_interfaces(data.num_layers - 1);
    std::partial_sum(d_ext.begin(), d_ext.end(), z_interfaces.begin());

    return z_interfaces;
  }

  [[nodiscard]] Eigen::Index layer_index(const real z0, const real zz) const {
    // returns the layer index corresponding to position zz
    a_real z_ref = interfaces(z0);

    if (zz < z0) {
      return 0;
    }

    // returns the first element for which element > value
    auto upper = std::upper_bound(z_ref.begin(), z_ref.end(), zz);

    return std::distance(z_ref.begin(), upper);
  }

  [[nodiscard]] complex index_profile(const real zz) const {
    const real z0 = 0;
    Eigen::Index i = layer_index(z0, zz);

    return data.n[i];
  }

  [[nodiscard]] complex solution(const a2n_complex coefficients,
                                 const a_complex p_vec, const real zz) const {
    const real z0 = 0;
    const Eigen::Index i = layer_index(z0, zz);
    const real z_ref = (i <= 0 ? z0 : interfaces(z0)[i - 1]);

    const complex p = p_vec[i];
    const complex A = coefficients(0, i);
    const complex B = coefficients(1, i);

    using namespace std::complex_literals;
    return A * std::exp(1.0i * data.k_vac * p * (zz - z_ref)) +
           B * std::exp(-1.0i * data.k_vac * p * (zz - z_ref));
  }

  [[nodiscard]] complex norm_solution(const a2n_complex &coefficients,
                                      const a_complex &p_vec,
                                      const a_complex &gamma) const {
    using namespace std::complex_literals;

    const a_complex a_vec = coefficients.row(0);
    const a_complex b_vec = coefficients.row(1);

    complex normsq = 0;
    normsq += 0.5i * squared(b_vec(0)) / (data.k_vac * p_vec(0) * gamma(0)) +
              0.5i * squared(a_vec(Eigen::last)) /
                  (data.k_vac * p_vec(Eigen::last) * gamma(Eigen::last));

    for (Eigen::Index l = 1; l < data.num_layers - 1; l++) {
      normsq +=
          2.0 * a_vec[l] * b_vec[l] * data.d[l - 1] / gamma[l] +
          0.5i / (data.k_vac * p_vec[l] * gamma[l]) *
              (squared(a_vec[l]) * (1.0 - std::exp(2.0i * data.k_vac *
                                                   p_vec[l] * data.d[l - 1])) -
               squared(b_vec[l]) * (1.0 - std::exp(-2.0i * data.k_vac *
                                                   p_vec[l] * data.d[l - 1])));
    }

    const real inv_wavelength = data.k_vac / (2 * std::numbers::pi);

    // length in units of wavelength
    return std::sqrt(inv_wavelength * normsq);
  }

  [[nodiscard]] complex solution_pos_te(const complex u, const real zz,
                                        const bool normalized,
                                        const bool force_guided) const {
    const a_complex p_vec = p_vec_fn(u);
    const a2n_complex coefficients =
        coefficients_pos(p_vec, data.mu, force_guided);

    complex norm = 1;

    if (normalized) {
      norm = norm_solution(coefficients, p_vec, data.mu);
    }

    return solution(coefficients, p_vec, zz) / norm;
  }

  [[nodiscard]] complex solution_pos_tm(const complex u, const real zz,
                                        const bool normalized,
                                        const bool force_guided) const {
    const a_complex p_vec = p_vec_fn(u);
    const a2n_complex coefficients =
        coefficients_pos(p_vec, data.eps, force_guided);

    complex norm = 1;

    if (normalized) {
      norm = norm_solution(coefficients, p_vec, data.eps);
    }

    return solution(coefficients, p_vec, zz) / norm;
  }

  [[nodiscard]] complex solution_neg_te(const complex u, const real zz,
                                        const bool normalized,
                                        const bool force_guided) const {
    const a_complex p_vec = p_vec_fn(u);
    const a2n_complex coefficients =
        coefficients_neg(p_vec, data.mu, force_guided);

    complex norm = 1;

    if (normalized) {
      norm = norm_solution(coefficients, p_vec, data.mu);
    }

    return solution(coefficients, p_vec, zz) / norm;
  }

  [[nodiscard]] complex solution_neg_tm(const complex u, const real zz,
                                        const bool normalized,
                                        const bool force_guided) const {
    const a_complex p_vec = p_vec_fn(u);
    const a2n_complex coefficients =
        coefficients_neg(p_vec, data.eps, force_guided);

    complex norm = 1;

    if (normalized) {
      norm = norm_solution(coefficients, p_vec, data.eps);
    }

    return solution(coefficients, p_vec, zz) / norm;
  }

  [[nodiscard]] complex greens_function_te(const real z_obs, const real z_src,
                                           const complex q) const {
    return greens_function_private(z_obs, z_src, q, data.mu);
  }

  [[nodiscard]] complex greens_function_tm(const real z_obs, const real z_src,
                                           const complex q) const {
    return greens_function_private(z_obs, z_src, q, data.eps);
  }

  [[nodiscard]] complex greens_function_private(const real z_obs,
                                                const real z_src,
                                                const complex q,
                                                const a_complex &gamma) const {
    const a_complex p_vec = p_vec_fn(q);
    const a2n_complex coeff_neg = coefficients_neg(p_vec, gamma, false);
    const a2n_complex coeff_pos = coefficients_pos(p_vec, gamma, false);

    complex numerator;
    if (z_obs > z_src) {
      numerator =
          solution(coeff_pos, p_vec, z_obs) * solution(coeff_neg, p_vec, z_src);
    } else {
      numerator =
          solution(coeff_neg, p_vec, z_obs) * solution(coeff_pos, p_vec, z_src);
    }

    const complex denominator =
        -data.k_vac * wronskian_fn_private(p_vec, gamma);

    return numerator / denominator;
  }

  [[nodiscard]] complex residue_te(const real z_obs, const real z_src,
                                   const complex q) const {
    return residue_private(z_obs, z_src, q, data.mu);
  }

  [[nodiscard]] complex residue_tm(const real z_obs, const real z_src,
                                   const complex q) const {
    return residue_private(z_obs, z_src, q, data.eps);
  }

  [[nodiscard]] complex residue_private(const real z_obs, const real z_src,
                                        const complex q,
                                        const a_complex &gamma) const {
    const a_complex p_vec = p_vec_fn(q);
    const a_complex diff_p_vec = diff_p_vec_fn(q);
    const a2n_complex coeff_neg = coefficients_neg(p_vec, gamma, false);
    const a2n_complex coeff_pos = coefficients_pos(p_vec, gamma, false);

    complex enumerator;
    if (z_obs > z_src) {
      enumerator =
          solution(coeff_pos, p_vec, z_obs) * solution(coeff_neg, p_vec, z_src);
    } else {
      enumerator =
          solution(coeff_neg, p_vec, z_obs) * solution(coeff_pos, p_vec, z_src);
    }

    const complex denominator =
        diff_wronskian_fn_private(p_vec, diff_p_vec, gamma);

    return enumerator / denominator;
  }

  StackData get_data() const { return data; }

  void set_data(const StackData &newdata) { data = newdata; }
};

}  // namespace model
